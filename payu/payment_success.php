<?php

/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 * zezwolenia.radbur.com.pl/payu/payment_success.php?sessionid=%sessionId%
 */
include('../config.php');
date_default_timezone_set('Etc/UTC');

require '../classes/PHPMailerAutoload.php';

$session_id = $_GET['sessionid']; // identyfikator sesji

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('../database.db');
    }
}

$db = new MyDB();

$result = $db->query('SELECT * FROM zezwolenia WHERE sessionid="'.$session_id.'"');
$array = $result->fetchArray();

$sessionid = $array['sessionid'];
$email = $array['email'];

if (empty($sessionid)) die("");

$mail = new PHPMailer();
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Debugoutput = 'html';
$mail->Host = $config['mail']['host'];
$mail->Port = $config['mail']['port'];
$mail->SMTPSecure = $config['mail']['SMTPSecure'];
$mail->SMTPAuth = $config['mail']['SMTPAuth'];
$mail->Username = $config['mail']['username'];
$mail->Password = $config['mail']['password'];
$mail->setFrom($config['mail']['username'], $config['mail']['Sender']);
$mail->addAddress($email, $email);
$mail->AddBCC($config['mail']['BCCMailAddress']);
$mail->Subject = $config['mail']['Subject'];
$mail->msgHTML(file_get_contents('./contents.html'), dirname(__FILE__));
$mail->AltBody = '';
$mail->addAttachment('../permissions/'.$sessionid.'.jpg');

//send the message, check for errors
if (!$mail->send()) {
    echo "Błąd wysyłki poczty: " . $mail->ErrorInfo ."<br/><hr/>";
}

//include('header.php');
include('html/head.inc.php')
?>

<body>
<div class="container">
	<div class="row">
		<div class="col-lg-6 col-sm-12">
			<a href="http://zezwolenia.radbur.com.pl/form.php"><img src="http://zezwolenia.radbur.com.pl/img/logo.jpg" height="62" style="margin-bottom:20px;"></a>
		</div>
		<div class="col-lg-6 col-sm-12">
			<font size="3"><b>e-zezwolenia</b> :zezwolenia.radbur.com.pl</font> <br/>
			radbur@radbur.com.pl | tel. /fax 58 684-11-86 <br/>
			tel. kom. 697-200-709
		</div>
	</div>

	<div class='row'>
		<div class='col-lg-12'>
			<div class="row rowtitle">
				<img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg">&nbsp;&nbsp;Regulamin/<a href="http://zezwolenia.radbur.com.pl/">wstecz</a>
			</div>
			
			<div>	
				<div id="rowtitle">
					<img src="http://zezwolenia.radbur.com.pl/img/usericon.jpg" style="float:left;">&nbsp;&nbsp;Dziękujemy! Życzymy połamania Kija!
				</div>
				<div>
					<p>Twoja płatność została przyjęta, jeżeli skorzystałeś z opcji przelewu tradycyjnego to musisz posiadać przy sobie potwierdzenie przelewu.</p>
					<br /><br />

					Zezwolenie dostępne do pobrania: <a href="http://zezwolenia.radbur.com.pl/permissions/<?= $sessionid ?>.jpg">POBIERZ ZEZWOLENIE</a>
					<p>Zezwolenie zostało wysłane na adres email: <?= $email ?></p>
				</div>
			</div>
	
			<div id="rowfooter">
				radbur@radbur.com.pl | tel. /fax 58 684-11-86 | tel. kom. 697-200-709<img src="http://zezwolenia.radbur.com.pl/img/payuicon.jpg" style="float:right;">
			</div>
		</div>
	</div>
</div>

<div id="footer">
	<center>Projekt i wykonanie: <a href="http://www.getgringo.pl/" style="color:white;">www.getgringo.pl</a></center>
</div>

</div>
</body>

</html>