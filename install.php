<?php

/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 * 
 * instalacja na serwerze
 * 
 */

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('database.db');
    }
}

$db = new MyDB();

$db->exec('CREATE TABLE zezwolenia (id INTEGER NOT NULL PRIMARY KEY, sessionid VARCHAR(100) NOT NULL, imie VARCHAR(100) NOT NULL, nazwisko VARCHAR(100) NOT NULL, email VARCHAR(100) NOT NULL)');
echo "Plik bazy został utworzony!"; 

?>