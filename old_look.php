<?php
/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 */
//error_reporting(E_ALL ^ E_NOTICE);
include('header.php');
?>

<head>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta charset="UTF-8">
<script type="text/javascript" src="js/liveValidation.js"></script>
<link rel="stylesheet" type="text/css" href="css/liveValidation.css" media="screen" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="js/jquery.ui.datepicker-pl.js"></script>

<!-- css -->
<link rel="stylesheet" type="text/css" href="css/forms.css" />
<meta name="viewport" content="width=device-width, initial-scale=1">


</head>

<body>
<div id="wrapper">
	<div class="leftColumn"><a href="http://zezwolenia.radbur.com.pl/form.php"><img src="http://zezwolenia.radbur.com.pl/img/logo.jpg" height="62" style="margin-bottom:20px;"></a></div>
    <div class="rightColumn"> <font size="3"><b>e-zezwolenia</b> :zezwolenia.radbur.com.pl</font><br>radbur@radbur.com.pl | tel. /fax 58 684-11-86 <br>tel. kom. 697-200-709
</div>
<br><br>

<div id='row'>
<form method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">
	<div id="rowtitle">
		<img src="http://zezwolenia.radbur.com.pl/img/usericon.jpg" style="float:left;">&nbsp;&nbsp;DANE WĘDKARZA
	</div>

<div id="row"><label>Imię:</label><input type="text" id="firstname" name="firstname" value="<?= $firstname ?>"></div>
	<div id="row"><label>Nazwisko: </label><input type="text" id="lastname" name="lastname" value="<?= $lastname ?>"></div>
	<div id="row"><label>Adres: </label><input type="text" id="address" name="address" value="<?= $address ?>"></div>
	<div id="row"><label>Kod pocztowy: </label><input type="text" id="postnumber" name="postnumber" value="<?= $postnumber ?>"></div>
	<div id="row"><label>Miasto: </label><input type="text" id="city" name="city" value="<?= $city ?>"></div>
	<div id="row"><label>Adres e-mail: </label><input type="text" id="email" name="email" value="<?= $email ?>"></div>
	<div id="row"><label>Powtórz adres e-mail: </label><input type="text" id="email_confirm" name="email_confirm" value="<?= $email_confirm ?>"></div>
    <div id="row"><label>Seria i nr DO: </label><input type="text" id="document" name="document" value="<?= $document ?>"></div>
	<div id="rowtitle"><img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg" style="float:left;">&nbsp;&nbsp;RODZAJ ZEZWOLENIA - <A href="http://zezwolenia.radbur.com.pl/cennik.php">Zobacz cennik </a></div>
	 
<div id="row"><label>Sezon: </label>
	<select id="period" name="period" onchange="this.form.submit()">
	  <option value="summer" <?php ($period=="summer"?print("selected='selected'"):"") ?>>Letni</option>
	  <option value="winter" <?php ($period=="winter"?print("selected='selected'"):"") ?>>Zimowy</option>
	</select> </div>

	<?php
	if ($period == "summer") { ?>
		<script>
	  $(function() {
		var d = new Date();
        $( "#date" ).datepicker( $.datepicker.regional[ "pl" ] );
		$('#date').datepicker({ minDate: "04/01/"+d.getFullYear(), maxDate: "11/30/"+d.getFullYear() });
	  });
	  </script>
	  
		<div id="row"><label>Sposób wędkowania: </label>
		<select id="fishing_type" name="fishing_type">
			<option value="0">2 wędki zwykłe z brzegu (1 jezioro)</option>
			<option value="1">2 wędki zwykłe z brzegu (2 jeziora)</option>
			<option value="2">Spinning lub 2 wędki zwykłe z brzegu (1 jezioro)</option>
			<option value="3">Spinning lub 2 wędki zwykłe z brzegu (2 jeziora)</option>
			<option value="4">2 wędki zwykłe z łodzi (1 jezioro)</option>
			<option value="5">2 wędki zwykłe z łodzi (2 jeziora)</option>
			<option value="6">Spinning lub 2 wędki z łodzi (1 jezioro)</option>
			<option value="7">Spinning lub 2 wędki z łodzi (2 jeziora)</option>
			<option value="8">Spławik lub spinning z brzegu (wszystkie jeziora)</option>
			<option value="9">Spławik lub spinning z łodzi (wszystkie jeziora)</option>
		</select> </div>
		
		<div id="row"><label>Okres wędkowania: </label>
		<select id="fishing_period" name="fishing_period">
			<option value="0">Cały sezon</option>
			<option value="1">1 miesiąc</option>
			<option value="2">14 dni</option>
			<option value="3">7 dni</option>
			<option value="4">3 dni</option>
			<option value="5">1 dzień</option>
		</select> </div>
		
		<div id="row"><label>Data rozpoczęcia wędkowania:</label>
		<input type="text" id="date" name="date"></div>
		
		<div id="row"><label>Wybór wód: </label></div>  
		<div id="rowcol"><input type="checkbox" class="lake" name="lake1" value="RADUŃSKIE DOLNE" />RADUŃSKIE DOLNE<br/>
		<input type="checkbox" class="lake" name="lake2" value="RADUŃSKIE GÓRNE" />RADUŃSKIE GÓRNE<br/>
		<input type="checkbox" class="lake" name="lake3" value="OSTRZYCKIE" />OSTRZYCKIE<br/>
		<input type="checkbox" class="lake" name="lake4" value="ŁĄCZYŃSKIE" />ŁĄCZYŃSKIE<br/>
		<input type="checkbox" class="lake" name="lake5" value="BRODNO WIELKIE" />BRODNO WIELKIE<br/>
		<input type="checkbox" class="lake" name="lake6" value="BRODNO MAŁE" />BRODNO MAŁE<br/>
		<input type="checkbox" class="lake" name="lake7" value="KŁODNO" />KŁODNO<br/>
		<input type="checkbox" class="lake" name="lake8" value="BIAŁE" />BIAŁE<br/>
		<input type="checkbox" class="lake" name="lake9" value="BUKSZYNO DUŻE" />BUKSZYNO DUŻE<br/>
		<input type="checkbox" class="lake" name="lake10" value="BUKSZYNO MAŁE" />BUKSZYNO MAŁE<br/>
		<input type="checkbox" class="lake" name="lake11" value="NIERZESTOWO" />NIERZESTOWO<br/>
		<input type="checkbox" class="lake" name="lake12" value="REKOWO" />REKOWO<br/></div>

	<?php } else if ($period == "winter") {?>
		<script>
	  $(function() {
		var d = new Date();
        $( "#date" ).datepicker( $.datepicker.regional[ "pl" ] );
		$('#date').datepicker({ minDate: "11/01/"+d.getFullYear(), maxDate: "03/31/2016" });
	  });
	  </script>
		<div id="row"><label>Sposób wędkowania: </label>
		<select id="fishing_type" name="fishing_type">
			<option value="10">1 wędka spod lodu lub 2 wędki z brzegu</option>
		</select> </div>
		
		<div id="row"><label>Okres wędkowania: </label>
		<select id="fishing_period" name="fishing_period">
			<option value="0">CAŁY SEZON 10 JEZIOR (spining)</option>
			<option value="1">Cały sezon</option>
			<option value="2">14 dni</option>
			<option value="3">7 dni</option>
			<option value="4">3 dni</option>
			<option value="5">1 dzień</option>
		</select> </div>	
		
		<div id="row"><label>Data rozpoczęcia wędkowania: </label>
		<input type="text" id="date" name="date"></div>

	<?php	
	}
	?>
 
	<div id="rowtitle">
		<center>
			<img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg" style="float:left;">&nbsp;&nbsp;ZAAKCEPTUJ REGULAMIN - <a href="http://zezwolenia.radbur.com.pl/regulamin.php">pokaż regulamin</a> - AKCEPTUJĘ REGULAMIN: <input type="checkbox" id="terms" name="terms" value="terms" />
			<input style="background:#141631;border:none;border-radius:4px;color:white;width:240px; height:50px;text-align:center;cursor:pointer;" type="submit" value="DALEJ - PŁATNOŚĆ ONLINE">
		</center>
	</div>
	</form>
</div>
	<br>

	
	<div id="row" style="float: none; !important">
		Uwaga, jeżeli płacisz przelewem tradycyjnym musisz posiadać przy sobie potwierdzenie przelewu, inaczej zezwolenie jest nie ważne. 
	</div>
	<div id="rowfooter">
		[DEV] radbur@radbur.com.pl | tel. /fax 58 684-11-86 | tel. kom. 697-200-709<img src="http://zezwolenia.radbur.com.pl/img/payuicon.jpg" style="float:right;">
	</div><br>

	<!-- VALIDATION -->
	<script type="text/javascript" src="js/formValidator.js"></script>
</div><br>
<center>Projekt i wykonanie: <a href="http://www.getgringo.pl/" style="color:white;">www.getgringo.pl</a></center>
</body>

</html>