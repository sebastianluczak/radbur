<?php
error_reporting(0);
/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 */

function check_in_range($start_date, $end_date, $date_from_user)
{
	// Convert to timestamp
	$start_ts = strtotime($start_date);
	$end_ts = strtotime($end_date);
	$user_ts = strtotime($date_from_user);

	// Check that user date is between start & end
	return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}

function checkSeason() {
	$winter_season_from = '2015-12-01';
	$winter_season_until = '2016-03-31';
	$now = date("Y-m-d");

	if (check_in_range($winter_season_from, $winter_season_until, $now) ) {
		return 'winter';
	} else {
		return 'summer';
	}
}

/*
*	Formularz
*/

function myFilter($var){
    if (is_array($var)) {
        if (count($var) >= 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    if (strlen($var)) return TRUE;
    
    return FALSE;
}

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$address = $_POST['address'];
$postnumber = $_POST['postnumber'];
$city = $_POST['city'];
$document = $_POST['document'];
$email = $_POST['email'];
$email_confirm = $_POST['email_confirm'];
$period = (empty($_POST['period'])?"":$_POST['period']);
$fishing_type = $_POST['fishing_type'];
$fishing_period = $_POST['fishing_period'];
$date = $_POST['date'];

$lakes = array( $_POST["lake1"], $_POST["lake2"], $_POST["lake3"], 
				$_POST["lake4"], $_POST["lake5"], $_POST["lake6"], 
				$_POST["lake7"], $_POST["lake8"], $_POST["lake9"], 
				$_POST["lake10"], $_POST["lake11"], $_POST["lake12"] );
$terms = $_POST['terms'];

if (count(array_filter($lakes)) == 0) {
	$lakes = array('Wszystkie, ');
}
				
$order_array = array( "firstname" => $firstname, "lastname" => $lastname, "address" => $address, 
						"postnumber" => $postnumber, "city" => $city, "email" => $email, 
						"email_confirm" => $email_confirm, "period" => $period, "document" => $document, "fishing_type" => $fishing_type, 
						"fishing_period" => $fishing_period, "date" => $date, "lakes" => array_filter($lakes), "terms" => $terms );
     
if ($_SERVER['REQUEST_METHOD'] == 'POST') {     
	if ( count(array_filter($order_array,'myFilter')) == 14 ) {
	    // process lakes
	    $lakes = "";
	    
	    foreach ($order_array['lakes'] as $lake) {
	        $lakes = $lakes." ".$lake.", ";
	    }
	    // remove trailing 2 chars;
	    $lakes = substr($lakes, 0, strlen($lakes)-2);
	    $order_array['lakes'] = $lakes;
	    
		$query = http_build_query($order_array);

		?>
		<form action='form_action.php' method='POST' name='frm'>
			<?php
			//echo '<head><meta name="description" content=""><meta name="keywords" content=""><meta name="author" content=""><meta charset="UTF-8">';
			foreach ($order_array as $a => $b) {
				echo "<input type='hidden' name='".$a."' value='".$b."'>";
			}
	        ?>
		</form>
		<script language="JavaScript">
			document.frm.submit();
		</script>
	<?php
	}
}
?>