<?php
/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 */
error_reporting(E_ALL ^ E_NOTICE);
include('header.php');
include('html/head.inc.php')
?>

<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-sm-12">
				<a href="http://zezwolenia.radbur.com.pl/form.php"><img src="http://zezwolenia.radbur.com.pl/img/logo.jpg" height="62" style="margin-bottom:20px;"></a>
			</div>
			<div class="col-lg-6 col-sm-12">
				<font size="3"><b>e-zezwolenia</b> :zezwolenia.radbur.com.pl</font> <br/>
				radbur@radbur.com.pl | tel. /fax 58 684-11-86 <br/>
				tel. kom. 697-200-709
			</div>
		</div>

		<div class='row'>
			<div class='col-lg-12'>

				<div class="row rowtitle">
					<img src="http://zezwolenia.radbur.com.pl/img/usericon.jpg">&nbsp;&nbsp;DANE WĘDKARZA
				</div>

				<form id='permission-form' method="POST" action="<?= $_SERVER['PHP_SELF'] ?>">

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Imię: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="firstname" name="firstname" value="<?= $firstname ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Nazwisko: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="lastname" name="lastname" value="<?= $lastname ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Adres: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="address" name="address" value="<?= $address ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Kod pocztowy: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="postnumber" name="postnumber" value="<?= $postnumber ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Miasto: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="city" name="city" value="<?= $city ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Adres e-mail: </label>
						<div class="col-sm-5">
							<input class="form-control" type="email" id="email" name="email" value="<?= $email ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Powtórz adres e-mail: </label>
						<div class="col-sm-5">
							<input class="form-control" type="email" id="email_confirm" name="email_confirm" value="<?= $email_confirm ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Seria i nr DO: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="document" name="document" value="<?= $document ?>" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="row rowtitle">
						<img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg" style="float:left;">&nbsp;&nbsp;RODZAJ ZEZWOLENIA - <a href="http://zezwolenia.radbur.com.pl/cennik.php">Zobacz cennik </a>
					</div>

					<div class="form-group-sm">
						<label class="col-sm-3 control-label">Rodzaj zezwolenia: </label>
						<div class="col-sm-5">
							<select class="form-control" id="period" name="period" required>
								<option>Wybierz sezon...</option>
								<?php if (checkSeason() == 'summer') { ?>
								<option value="summer" <?php ($period=="summer"?print("selected='selected'"):"") ?>>Letni</option>
								<?php } elseif (checkSeason() == 'winter') { ?>
								<option value="winter" <?php ($period=="winter"?print("selected='selected'"):"") ?>>Zimowy</option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm" id='fishing_type-container' style='display: none'>
						<label class="col-sm-3 control-label">Sposób wędkowania: </label>
						<div class="col-sm-5">
							<select class="form-control" id="fishing_type" name="fishing_type" required>

							</select>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm" id='fishing_period-container' style='display: none'>
						<label class="col-sm-3 control-label">Okres wędkowania: </label>
						<div class="col-sm-5">
							<select class="form-control" id="fishing_period" name="fishing_period" required>

							</select>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm" id='date-container' style='display: none'>
						<label class="col-sm-3 control-label">Data rozpoczęcia wędkowania: </label>
						<div class="col-sm-5">
							<input class="form-control" type="text" id="date" name="date" required>
						</div>
						<div class="col-sm-4 error-status">

						</div>
					</div>

					<div class="form-group-sm" id='lake-container' style='display: none'>
						<label class="col-lg-12 control-label">Wybór wód: </label>

						<div class="col-lg-12">
							<div class='col-lg-4'>
								<input type="checkbox" class="lake" name="lake1" value="RADUŃSKIE DOLNE" />&nbsp; RADUŃSKIE DOLNE<br/>
								<input type="checkbox" class="lake" name="lake2" value="RADUŃSKIE GÓRNE" />&nbsp; RADUŃSKIE GÓRNE<br/>
								<input type="checkbox" class="lake" name="lake3" value="OSTRZYCKIE" />&nbsp; OSTRZYCKIE<br/>
								<input type="checkbox" class="lake" name="lake4" value="ŁĄCZYŃSKIE" />&nbsp; ŁĄCZYŃSKIE<br/>
							</div>
							<div class='col-lg-4'>
								<input type="checkbox" class="lake" name="lake5" value="BRODNO WIELKIE" />&nbsp; BRODNO WIELKIE<br/>
								<input type="checkbox" class="lake" name="lake6" value="BRODNO MAŁE" />&nbsp; BRODNO MAŁE<br/>
								<input type="checkbox" class="lake" name="lake7" value="KŁODNO" />&nbsp; KŁODNO<br/>
								<input type="checkbox" class="lake" name="lake8" value="BIAŁE" />&nbsp; BIAŁE<br/>
							</div>
							<div class='col-lg-4'>
								<input type="checkbox" class="lake" name="lake9" value="BUKSZYNO DUŻE" />&nbsp; BUKSZYNO DUŻE<br/>
								<input type="checkbox" class="lake" name="lake10" value="BUKSZYNO MAŁE" />&nbsp; BUKSZYNO MAŁE<br/>
								<input type="checkbox" class="lake" name="lake11" value="NIERZESTOWO" />&nbsp; NIERZESTOWO<br/>
								<input type="checkbox" class="lake" name="lake12" value="REKOWO" />&nbsp; REKOWO<br/>
							</div>
						</div>
					</div>  

					<div class="row rowtitle">
						<div class='col-lg-12'>
							<img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg" style="float:left;">&nbsp;&nbsp;ZAAKCEPTUJ REGULAMIN - <a href="http://zezwolenia.radbur.com.pl/regulamin.php">pokaż regulamin</a> - AKCEPTUJĘ REGULAMIN: <input type="checkbox" id="terms" name="terms" value="terms" required/>
						</div>
					</div>

					<div class='row'>
						<div class='col-lg-12'>
						<center>
							<input class='btn btn-primary btn-formsubmit' type="submit" value="DALEJ - PŁATNOŚĆ ONLINE">
						</center>
						</div>
					</div>

				</form>
			</div>

			<div id="row">
				Uwaga, jeżeli płacisz przelewem tradycyjnym musisz posiadać przy sobie potwierdzenie przelewu, inaczej zezwolenie jest nie ważne. 
			</div>
		</div>

		<div id="rowfooter">
			radbur@radbur.com.pl | tel. /fax 58 684-11-86 | tel. kom. 697-200-709<img src="http://zezwolenia.radbur.com.pl/img/payuicon.jpg" style="float:right;">
		</div>
	</div>


	<div id="footer">
		<center>Projekt i wykonanie: <a href="http://www.getgringo.pl/" style="color:white;">www.getgringo.pl</a></center>
	</div>
</body>

<?php
include('html/bottom_js.inc.php');
?>

</html>