<?php
/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 */
include('../config.php');
 
class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('../database.db');
    }
}

$db = new MyDB();

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$address = $_POST['address'];
$postnumber = $_POST['postnumber'];
$city = $_POST['city'];
$email = $_POST['email'];
$email_confirm = $_POST['email_confirm'];
$period = (empty($_POST['period'])?"summer":$_POST['period']);
$fishing_type = $_POST['fishing_type'];
$fishing_period = $_POST['fishing_period'];
$date = $_POST['date'];
$lakes = $_POST['lakes'];
$terms = $_POST['terms'];
$document = $_POST['document'];
$file_id = $_POST['sessionid'];
$date_now =  date('d/m/Y');

$query = 'SELECT * FROM zezwolenia WHERE sessionid ="'.$file_id.'"';
$result = $db->query($query);
$array = $result->fetchArray();

// try to calculate permission id

$id_permission = $array['id'] - 578;

// cennik, dość skomplikowana struktura ale odwołania potem za pomocą prostego array_matrix[sposob_wedkowania][okres_wedkowania] <3
$price_matrix = array ( array("200","90","75","60","30","10") ,
                        array("250","110","90","75","40","15") ,
                        array("250","150","125","80","60","30") ,
                        array("300","170","140","100","70","40") ,
                        array("300","190","150","115","90","45") ,
                        array("350","210","170","130","110","50") ,
                        array("350","210","190","140","115","50") ,
                        array("400","230","210","160","130","60") ,
                        array("450","300","210","190","150","70") ,
                        array("500","370","300","250","170","90") ,
                        array("230","130","80","70","30","10") );
                        
// konwersja sposobu na nazwę
$fishing_type_matrix = array( "2 wędki zwykłe z brzegu (1 jezioro)" ,
		"2 wędki zwykłe z brzegu (2 jeziora)" ,
        "Spinning lub 2 wędki zwykłe z brzegu (1 jezioro)" ,
        "Spinning lub 2 wędki zwykłe z brzegu (2 jeziora)" ,
        "2 wędki zwykłe z łodzi (1 jezioro)" ,
        "2 wędki zwykłe z łodzi (2 jeziora)" ,
        "Spinning lub 2 wędki z łodzi (1 jezioro)" ,
        "Spinning lub 2 wędki z łodzi (2 jeziora)" ,
        "Spławik lub spinning z brzegu (wszystkie jeziora)" ,
        "Spławik lub spinning z łodzi (wszystkie jeziora)" ,
        "1 wędka spod lodu lub 2 wędki z brzegu");

// konwersja okresu na nazwę
if ($fishing_type == 10) {
    $fishing_period_matrix = array(	"CAŁY SEZON 10 JEZIOR",
		"Cały sezon",
        "14 dni",
        "7 dni",
        "3 dni",
        "1 dzień");
} else {
    $fishing_period_matrix = array(	"Cały sezon",
		"1 miesiąc",
        "14 dni",
        "7 dni",
        "3 dni",
        "1 dzień");
}			
$order_array = array( "firstname" => $firstname, "lastname" => $lastname, "address" => $address, 
						"postnumber" => $postnumber, "city" => $city, "email" => $email, 
						"email_confirm" => $email_confirm, "period" => $period, "fishing_type" => $fishing_type_matrix[$fishing_type], "document" => $document,
						"fishing_period" => $fishing_period_matrix[$fishing_period], "date" => $date, "lakes" => $lakes, "terms" => $terms );
					
//header("Content-Type: image/jpeg");
$im = ImageCreateFromJpeg("zezwolenie_szkic.jpg"); 
$black = ImageColorAllocate($im, 0, 0, 0);

$permission_number = $id_permission."/2016";
$name = $order_array['firstname']." ".$order_array['lastname'];
$address = $order_array['address'];
$permission_document = $order_array['document'];
$permission_lakes = $order_array['lakes'];
if ( ($fishing_type == 8 ) || ($fishing_type == 9) ) $permission_lakes = "Wszystkie jeziora"; 
$permission_method = $order_array['fishing_type'];
$permission_duration = $order_array['fishing_period'];
$permission_price = $price_matrix[$fishing_type][$fishing_period];
$permission_dateFrom = $order_array['date'];

$text_array = array( "permission_number" => $permission_number,
						"name" => $name,
						"address" => $address." ".$postnumber." ".$city,
						"permission_lakes" => $permission_lakes,
						"permission_method" => $permission_method,
						"permission_duration" => $permission_duration,
						"permission_price" => $permission_price,
						"permission_dateFrom" => $permission_dateFrom,
                        "document" => $permission_document,
                        "permission_date" => $date_now );
						
$position_array = array ( "permission_number" => array( "x" => "1480", "y" => "244" ),
							"name" => array( "x" => "1210", "y" => "305" ),
							"address" => array( "x" => "1066", "y" => "357" ),
							"permission_lakes" => array( "x" => "990", "y" => "468" ),
							"permission_method" => array( "x" => "990", "y" => "576" ),
							"permission_duration" => array( "x" => "990", "y" => "694" ),
							"permission_price" => array( "x" => "1539", "y" => "774" ),
							"permission_dateFrom" => array( "x" => "1390", "y" => "873" ),
                            "document" => array( "x" => "990", "y" => "930" ),
                            "permission_date" => array( "x" => "1060", "y" => "1120" ) );

$i=0;
foreach ($position_array as $position) {
	$temp_key = array_keys($position_array);
	$current_key = $temp_key[$i];	
	Imagettftext($im, 20, 0, $position["x"], $position["y"], $black, './verdana.ttf', $text_array[$current_key]);
	$i++;
}
Imagejpeg($im, '../permissions/'.$file_id.'.jpg', 80);
ImageDestroy($im);
?> 