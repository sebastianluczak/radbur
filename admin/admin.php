<?php

/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 */



class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('../database.db');
    }
}

$db = new MyDB();

if (isset($_GET['delete'])) {
    $q = 'delete from zezwolenia';
    $db->query($q);
    header('Location: '.$_SERVER['PHP_SELF']); 
}
include('../html/head.inc.php')
?>

<body>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <a href="http://zezwolenia.radbur.com.pl/form.php"><img src="http://zezwolenia.radbur.com.pl/img/logo.jpg" height="62" style="margin-bottom:20px;"></a>
        </div>
        <div class="col-lg-6 col-sm-12">
            <font size="3"><b>e-zezwolenia</b> :zezwolenia.radbur.com.pl</font> <br/>
            radbur@radbur.com.pl | tel. /fax 58 684-11-86 <br/>
            tel. kom. 697-200-709
        </div>
    </div>

    <div class='row'>
        <div class='col-lg-12'>
            <div class="row rowtitle">
                <img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg">&nbsp;&nbsp;Lista/<a href="http://zezwolenia.radbur.com.pl/">wstecz</a>
            </div>

            <div>
            	<table class="table-responsive" border="1" style="color:#000; font-size:12px; padding:10px;border:none;">
                	<tr style="border:none;">
                    	<th>id</th>
                    	<th>imie</th>
                    	<th>nazwisko</th>
                    	<th>email</th>
                        <th>data</th>
                        <th>lakes</th>
                        <th>dokument</th>
                    	<th>link do zezwolenia</th>
                	</tr>

                    <?php

                    $result = $db->query('SELECT * FROM zezwolenia ORDER BY id DESC');
                    while ($array = $result->fetchArray()) {
                        echo "<tr><td>".$array['id']."</td>
                        <td>".$array['imie']."</td>
                        <td>".$array['nazwisko']."</td>
                        <td>".$array['email']."</td>
                        <td>".$array['date']."</td>
                        <td>".$array['lakes']."</td>
                        <td>".$array['document']."</td>
                        <td><a href='http://zezwolenia.radbur.com.pl/permissions/".$array['sessionid'].".jpg'>LINK</a></td></tr>";
                    }

                    ?>
                </table>
                    
                <hr/>
                Usuń wszystkie wpisy zezwolenia: <a href="?delete=yes">USUŃ</a>
            </div>

            <div id="rowfooter">
                radbur@radbur.com.pl | tel. /fax 58 684-11-86 | tel. kom. 697-200-709<img src="http://zezwolenia.radbur.com.pl/img/payuicon.jpg" style="float:right;">
            </div>
        </div>
    </div>
</div>

<div id="footer">
    <center>Projekt i wykonanie: <a href="http://www.getgringo.pl/" style="color:white;">www.getgringo.pl</a></center>
</div>

</div>
</body>

</html>