var firstname = new LiveValidation('firstname', { validMessage: "Dane są poprawne", wait: 500 });
firstname.add( Validate.Length, { minimum: 3, tooShortMessage: "co najmniej 3 znaki" } );
firstname.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );

var lastname = new LiveValidation('lastname', { validMessage: "Dane są poprawne", wait: 500 });
lastname.add( Validate.Length, { minimum: 2, tooShortMessage: "co najmniej 2 znaki" } );
lastname.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );

var document = new LiveValidation('document', { validMessage: "Dane są poprawne", wait: 500 });
city.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );

var address = new LiveValidation('address', { validMessage: "Dane są poprawne", wait: 500 });
address.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );

var postnumber = new LiveValidation('postnumber', { validMessage: "Dane są poprawne", wait: 500 });
postnumber.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );
postnumber.add( Validate.Format, { pattern: /[0-9]{2}-[0-9]{3}/, failureMessage: "Błędny kod, wprowadź prawidłowy kod np. 31-300" } );

var city = new LiveValidation('city', { validMessage: "Dane są poprawne", wait: 500 });
city.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );

var email = new LiveValidation('email', { validMessage: "Dane są poprawne", wait: 500 });
email.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );
email.add( Validate.Email, { failureMessage: "Adres email nieprawidłowy" } );

var email_confirm = new LiveValidation('email_confirm', { validMessage: "Dane są poprawne", wait: 500 });
email_confirm.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );
email_confirm.add( Validate.Email, { failureMessage: "Adres email nieprawidłowy" } );

var date = new LiveValidation('date', { validMessage: "Dane są poprawne", wait: 500 });
date.add( Validate.Presence, { failureMessage: "Pole nie może być puste" } );

var terms = new LiveValidation('terms', { validMessage: "Dane są poprawne", wait: 500 });
terms.add( Validate.Acceptance , { failureMessage: "Pole nie może być puste" } );

var limit_lakes = 1;

$('input.lake').on('change', function(evt) {
    if (limit_lakes == null) { limit_lakes = 20; $("input:checkbox").attr('checked', true); }
   if($(this).siblings(':checked').length >= limit_lakes) {
       this.checked = false;
       alert("Możesz zaznaczyć maksimum " + limit_lakes);
   }
});

$('#date').on('change', function(evt) {
    $('#date').focus();
    $('#date').blur();
});

$('#fishing_type').on('change', function(evt) {
    $("input:checkbox").attr('checked', false);
function get_numbers(input) {
    return input.match(/[0-9]+/g);
}

   text = $("#fishing_type option:selected").text();
   var limit = text.match(/\((.+)\)/);
   var first_test = get_numbers(limit[0]);
   limit_lakes = first_test;
   if (limit_lakes == null) { limit_lakes = 20; $("input:checkbox").attr('checked', true); }
});