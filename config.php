<?php

/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 * 
 * config.php
 */

$config = array();
// ścieżka do img_workera
$config['img_worker']['worker_location'] = 'http://zezwolenia.radbur.com.pl/img_worker/worker.php';

/**
 * 
 * Konfiguracja maila
 * 
 */
$config['mail']['host'] = 'smtp.gmail.com'; // adres serwera SMTP
$config['mail']['port'] = 587; // port serwera SMTP
$config['mail']['SMTPSecure'] = 'tls'; // sposób autoryzacji SMTP
$config['mail']['SMTPAuth'] = true; // czy serwer używa autoryzacji SMTP
$config['mail']['username'] = "zezwoleniaradbur@gmail.com"; // login serwera pocztowego
$config['mail']['password'] = "admin1989"; // hasło do serwera pocztowego
$config['mail']['Subject'] = 'e-Zezwolenia'; // temat wiadomości email
$config['mail']['Sender'] = "e-Zezwolenia"; // nazwa nadawcy wiadomości email
$config['mail']['BCCMailAddress'] = "zezwoleniaradbur@gmail.com"; // adres e-mail wysyłki kopii BCC maila z zezwoleniem
?>