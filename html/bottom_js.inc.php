<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/localization/messages_pl.js"></script>

<script>

(function($, window) {
  $.fn.replaceOptions = function(options) {
    var self, $option;

    this.empty();
    self = this;

    $.each(options, function(index, option) {
      $option = $("<option></option>")
        .attr("value", option.value)
        .text(option.text);
      self.append($option);
    });
  };
})(jQuery, window);

jQuery.validator.addMethod("zipcode", function(value, element) {
  return this.optional(element) || /^\d{2}-\d{3}?$/.test(value);
}, "Wprowadź prawidłowy kod pocztowy.");

jQuery.validator.addMethod("document", function(value, element) {
  return this.optional(element) || /^[A-z]{2}/.test(value);
}, "Wprowadź prawidłowy numer dokumentu.");

$("#permission-form").validate({
	rules: {
        firstname: {
            minlength: 3,
            maxlength: 25,
            required: true
        },
        lastname: {
            minlength: 3,
            maxlength: 25,
            required: true
        },
        address: {
        	minlength: 6,
        	maxlength: 40,
        	required: true
        },
        postnumber: {
        	zipcode: true,
        },
        city: {
        	minlength: 2,
            maxlength: 25,
            required: true
        },
        document: {
        	document: true,
        }

    },
    messages: {
        firstname: { required: "Wprowadź imię" },
    	lastname: { required: "Wprowadź nazwisko" },
    	address: { required: "Wprowadź prawidłowy adres" },
        postnumber: { required: "Wprowadź prawidłowy kod pocztowy" },    
        city: { required: "Wprowadź miasto" },

    },
    validClass: "has-success",
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
    	var placeholder = element.closest('.form-group-sm').find('.error-status');
        error.insertAfter(placeholder);
    },
    highlight: function(element) {
        $(element).closest('.form-group-sm').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group-sm').removeClass('has-error').addClass('has-success');
    },
    success: function(element) {  
        var placeholder = element.closest('.form-group-sm').find('.error-status');
    },
    onkeyup: function(element) { $(element).valid(); },
    onfocusout: function(element) { $(element).valid(); },
});

$('#period').on('change', function(_ev) {

	if (this.value == 'summer') {
		var options = [
			{text: "2 wędki zwykłe z brzegu (1 jezioro)", value: 0},
			{text: "2 wędki zwykłe z brzegu (2 jeziora)", value: 1},
			{text: "Spinning lub 2 wędki zwykłe z brzegu (1 jezioro)", value: 2},
			{text: "Spinning lub 2 wędki zwykłe z brzegu (2 jeziora)", value: 3},
			{text: "2 wędki zwykłe z łodzi (1 jezioro)", value: 4},
			{text: "2 wędki zwykłe z łodzi (2 jeziora)", value: 5},
			{text: "Spinning lub 2 wędki z łodzi (1 jezioro)", value: 6},
			{text: "Spinning lub 2 wędki z łodzi (2 jeziora)", value: 7},
			{text: "Spławik lub spinning z brzegu (wszystkie jeziora)", value: 8},
			{text: "Spławik lub spinning z łodzi (wszystkie jeziora)", value: 9},
		];
		$('#fishing_type').replaceOptions(options);

		var options = [
			{text: "Cały sezon", value: 0},
			{text: "1 miesiąc", value: 1},
			{text: "14 dni", value: 2},
			{text: "7 dni", value: 3},
			{text: "3 dni", value: 4},
			{text: "1 dzień", value: 5}
		];
		$('#fishing_period').replaceOptions(options);

		var d = new Date();
        $( "#date" ).datepicker( $.datepicker.regional[ "pl" ] );
		$('#date').datepicker({ minDate: "04/01/"+d.getFullYear(), maxDate: "11/30/"+d.getFullYear() });

		$("#fishing_type-container, #fishing_period-container, #date-container, #lake-container").show();
	} else if (this.value == 'winter') {
		var options = [
		  {text: "1 wędka spod lodu lub 2 wędki z brzegu", value: 10}
		];
		$('#fishing_type').replaceOptions(options);

		var options = [
			{text: "CAŁY SEZON 10 JEZIOR (spining)", value: 0},
			{text: "Cały sezon", value: 1},
			{text: "14 dni", value: 2},
			{text: "7 dni", value: 3},
			{text: "3 dni", value: 4},
			{text: "1 dzień", value: 5}
		];
		$('#fishing_period').replaceOptions(options);

		var d = new Date();
		$( "#date" ).datepicker( $.datepicker.regional[ "pl" ] );
		$('#date').datepicker({ minDate: "11/01/"+d.getFullYear(), maxDate: "03/31/2016" });

		$("#fishing_type-container, #fishing_period-container, #date-container, #lake-container").show();
	} else {
		$("#fishing_type-container, #fishing_period-container, #date-container, #lake-container").hide();
	}
});


</script>