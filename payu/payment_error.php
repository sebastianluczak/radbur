<?php

/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 * 
 * <strong>payment_error.php</strong>
 */

// this file is for handling error from PayU
$error_array = array(
100 =>	"brak lub błędna wartość parametru pos_id",
101 =>	"brak parametru session_id",
102	=>	"brak parametru ts",
103	=>	"brak lub błędna wartość parametru sig",
104	=>	"brak parametru desc",
105	=>	"brak parametru client_ip",
106	=>	"brak parametru first_name",
107	=>	"brak parametru last_name",
108	=>	"brak parametru street",
109	=>	"brak parametru city",
110	=>	"brak parametru post_code",
111	=>	"brak parametru amount",
112	=>	"błędny numer konta bankowego",
113	=>	"brak parametru email",
114	=>	"brak numeru telefonu",
200	=>	"inny chwilowy błąd",
201	=>	"inny chwilowy błąd bazy danych",
202	=>	"POS o podanym identyfikatorze jest zablokowany",
203	=>	"niedozwolona wartość pay_type dla danego parametru pos_id",
204	=>	"podana metoda płatności (wartość pay_type) jest chwilowo zablokowana dla danego parametru pos_id, np. przerwa konserwacyjna bramki płatniczej",
205	=>	"kwota transakcji mniejsza od wartości minimalnej",
206	=>	"kwota transakcji większa od wartości maksymalnej",
207	=>	"przekroczona wartość wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym",
208	=>	"POS działa w wariancie ExpressPayment lecz nie nastąpiła aktywacja tego wariantu współpracy (czekamy na zgodę działu obsługi klienta)",
209	=>	"błędny numer pos_id lub pos_auth_key",
211	=>	"nieprawidłowa waluta transakcji",
212	=>	"próba utworzenia transakcji częściej niż raz na minutę - dla nieaktywnej firmy",
500	=>	"transakcja nie istnieje",
501	=>	"brak autoryzacji dla danej transakcji",
502	=>	"transakcja rozpoczęta wcześniej",
503	=>	"autoryzacja do transakcji była już przeprowadzana",
504	=>	"transakcja anulowana wcześniej",
505	=>	"transakcja przekazana do odbioru wcześniej",
506	=>	"transakcja już odebrana",
507	=>	"błąd podczas zwrotu środków do Klienta",
508	=>	"Klient zrezygnował z płatności",
599	=>	"błędny stan transakcji, np. nie można uznać transakcji kilka razy lub inny, prosimy o kontakt",
999	=>	"inny błąd krytyczny - prosimy o kontakt",
777	=>	"utworzenie transakcji spowoduje przekroczenie limitu transakcji dla firmy w trakcie weryfikacji, weryfikacja odbędzie się w ciągu jednego dnia roboczego" );

$error_description = $error_array[$_GET['error']];

include('html/head.inc.php')
?>

<body>
<div class="container">
	<div class="row">
		<div class="col-lg-6 col-sm-12">
			<a href="http://zezwolenia.radbur.com.pl/form.php"><img src="http://zezwolenia.radbur.com.pl/img/logo.jpg" height="62" style="margin-bottom:20px;"></a>
		</div>
		<div class="col-lg-6 col-sm-12">
			<font size="3"><b>e-zezwolenia</b> :zezwolenia.radbur.com.pl</font> <br/>
			radbur@radbur.com.pl | tel. /fax 58 684-11-86 <br/>
			tel. kom. 697-200-709
		</div>
	</div>

	<div class='row'>
		<div class='col-lg-12'>
			<div class="row rowtitle">
				<img src="http://zezwolenia.radbur.com.pl/img/daneicon.jpg">&nbsp;&nbsp;Regulamin/<a href="http://zezwolenia.radbur.com.pl/">wstecz</a>
			</div>
			
			<div>	
				<div id="rowtitle">
					<img src="http://zezwolenia.radbur.com.pl/img/usericon.jpg" style="float:left;">&nbsp;&nbsp;Błąd transakcji PayU
				</div>
				<div>
					<p>Komunikat błędu: <b><?= $error_description ?></b></p>
					<p>Transakcja zakończona niepowodzeniem, spróbuj jeszcze raz klikając <a href="http://zezwolenia.radbur.com.pl/">ten link.</a></p>
				</div>
			</div>
	
			<div id="rowfooter">
				radbur@radbur.com.pl | tel. /fax 58 684-11-86 | tel. kom. 697-200-709<img src="http://zezwolenia.radbur.com.pl/img/payuicon.jpg" style="float:right;">
			</div>
		</div>
	</div>
</div>

<div id="footer">
	<center>Projekt i wykonanie: <a href="http://www.getgringo.pl/" style="color:white;">www.getgringo.pl</a></center>
</div>

</div>
</body>

</html>