<html>
<head>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta charset="UTF-8">
</head>
<body>
<?php
/**
 * @author Sebastian Łuczak <sebastian.m.luczak@gmail.com>
 * @copyright 2014
 */
include("./config.php");
 
class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('database.db');
    }
}

$db = new MyDB();

/* generowanie dokumentu i/lub generowanie płatności. */
$arrayOfData = $_POST;
$PAYU_ENABLED = 1;
$WORKERSCRIPT_LOCATION = $config['img_worker']['worker_location'];

function getPrice($sposob_wedkowania,$okres_wedkowania){
    $price_matrix = array ( array("200","90","75","60","30","10") ,
                        array("250","110","90","75","40","15") ,
                        array("250","150","125","80","60","30") ,
                        array("300","170","140","100","70","40") ,
                        array("300","190","150","115","90","45") ,
                        array("350","210","170","130","110","50") ,
                        array("350","210","190","140","115","50") ,
                        array("400","230","210","160","130","60") ,
                        array("450","300","210","190","150","70") ,
                        array("500","370","300","250","170","90") ,
                        array("150","80","50","25","10","250") );
    return $price_matrix[$sposob_wedkowania][$okres_wedkowania];
}

function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
{
    $str = '';
    $count = strlen($charset);
    while ($length--) {
        $str .= $charset[mt_rand(0, $count-1)];
    }
    return $str;
}

function curl_grab_page($url,$data,$secure="false",$ref_url="",$login = "false",$proxy = "null",$proxystatus = "false"){
    if($login == 'true') {
        $fp = fopen("cookie.txt", "w");
        fclose($fp);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_COOKIEJAR, "cookie.txt");
    curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt");

    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    if ($proxystatus == 'true') {
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, TRUE);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
    }
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

    if($secure=='true')
    {
        curl_setopt($ch, CURLOPT_SSLVERSION,3);
    }

    curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Expect:' ) );


    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
    curl_setopt($ch, CURLOPT_REFERER, $ref_url);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    ob_start();

    return curl_exec ($ch); // execute the curl command

    curl_getinfo($ch);
    ob_end_clean();
    curl_close ($ch);
    unset($ch);
}

if (empty($arrayOfData)) {
	die("empty");
} else {
	if ($PAYU_ENABLED == 1){
        $arrayOfData['sessionid'] = randString(10);
        $now = date('Y-m-d H:i:s');
        $query = 'INSERT INTO zezwolenia VALUES (NULL, "'.$arrayOfData['sessionid'].'", "'.$arrayOfData['firstname'].'","'.$arrayOfData['lastname'].'","'.$arrayOfData['email'].'", "'.$now.'", "'.$arrayOfData['lakes'].'", "'.$arrayOfData['document'].'")';
        $db->exec($query);
		//header("Content-Type: image/jpeg");
		//Imagejpeg($data, '', 80);
        $price = getPrice($arrayOfData['fishing_type'],$arrayOfData['fishing_period']);
        $data = curl_grab_page($WORKERSCRIPT_LOCATION, $arrayOfData);

        ?>
        
        <form action="https://secure.payu.com/paygw/UTF/NewPayment" method="GET" name="payform">
    <input type="hidden" name="first_name" value="<?= $arrayOfData['firstname'] ?>"><br/>
    <input type="hidden" name="last_name" value="<?= $arrayOfData['lastname'] ?>"><br/>
    <input type="hidden" name="email" value="<?= $arrayOfData['email'] ?>"><br/>
    <input type="hidden" name="pos_id" value="166532">
    <input type="hidden" name="pos_auth_key" value="m2JHMnQ">
    <input type="hidden" name="session_id" value="<?= $arrayOfData['sessionid'] ?>">
    <input type="hidden" name="amount" value="<?= $price*100 ?>">
    <input type="hidden" name="desc" value="eZezwolenie">
    <input type="hidden" name="client_ip" value="<?= $_SERVER['REMOTE_ADDR'] ?>">
    <input type="hidden" name="js" value="0">
    <input type="hidden" value="Zapłać poprzez PayU">
</form>
<script language="JavaScript" type="text/javascript">
    <!--
    document.forms['payform'].js.value=1;
    -->
</script>
	<script language="JavaScript">
		document.payform.submit();
	</script>
        
        <?php
        
	} else {
		die("Nie skonfigurowano PayU!");
	}
}
?>

</body>
</html>